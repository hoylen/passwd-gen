/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE.
 */

import 'package:passwd_gen/passwd_gen.dart';

/// Generates a 12 character long password with a collection
/// of basic Latin characters and custom distribution between collections
void main() {
  final generator = PasswordService.latinBase();
  generator.getCollectionKeys().forEach(print);
  /*
    Output of previous line:
      latinAlphabetLowerCase
      latinAlphabetUpperCase
      latinNumbers
      specialCharacters
  */
  final dist = <String,int>{};
  dist.putIfAbsent('latinAlphabetLowerCase', () => 4);
  dist.putIfAbsent('latinAlphabetUpperCase', () => 5);
  dist.putIfAbsent('latinNumbers', () => 3);
  // dist.putIfAbsent('specialCharacters', () => 0);
  final password=generator.generatePassword(distribution: dist);
  print('Password with 4 lower case, 5 upper case, 3 numbers, '
      '0 special chars : $password');
}