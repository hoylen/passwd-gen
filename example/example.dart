/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE.
 */

import 'package:passwd_gen/passwd_gen.dart';

/// Generates a 29 character long password with a collection
/// of basic Latin characters.
void main() {
  final generator = PasswordService.latinBase();
  print('Password with length of 29; upper,lower case, numbers, special '
      'characters that are available with UK/US keyboard: ${generator(29)}');
}