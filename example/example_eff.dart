/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE.
 */

import 'package:passwd_gen/passwd_gen.dart';

/// Generates a 5 words long passphrase using EFF's words.
void main() {
  final generator = PasswordService.effLargeListWords();
  print('Passphrase based on EFF large words list: ${generator(5)}');
}