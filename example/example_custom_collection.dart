/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE.
 */

import 'package:passwd_gen/passwd_gen.dart';

/// Generates a 20 character long password with a collection
/// of custom KATAKANA characters.
void main() {
  final generator = PasswordService();
  generator.addCollectionOfPasswordItems('KATAKANA', [
    'ァ','ア','ィ','イ','ゥ','ウ','ェ','エ','ォ',
    'オ','カ','ガ','キ','ク','グ','ケ','ゲ','コ','ゴ','サ','ザ','シ','ジ','ス','ズ',
    'セ','ゼ','ソ','ゾ','タ','ダ','チ','ヂ','ッ','ツ','ヅ','テ','デ','ト',
    'ド','ナ','ニ','ヌ','ネ','ノ','ハ','バ','パ','ヒ','ビ','ピ','フ','ブ','プ',
    'ヘ','ベ','ペ','ホ','ボ','ポ','マ','ミ','ム','メ','モ','ャ','ヤ','ュ','ユ','ョ',
    'ヨ','ラ','リ','ル','レ','ロ','ワ','ヰ','ヱ','ヲ','ン','ヴ','ヵ','ヶ','ヷ','ヸ',
    'ヹ','ヺ','・','ー','ヽ','ヾ','ヿ']);
  final password=generator.generatePassword(length: 20);
  print('Password using custom set : $password');
}