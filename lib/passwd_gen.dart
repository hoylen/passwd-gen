/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE.
 */

/// A library to generate passwords. It includes a series of collections
/// of items such as:
/// Latin characters as well as French, German, Italian, Spanish extensions and
/// the usual special characters.
/// It also includes the EFF's word list from the work
/// of Joseph Bonneau and others. (See references below).
/// It allows to extend the collections with its own elements.

library passwd_gen;

import 'dart:math';

part 'src/eff_large_wordlist.dart';
part 'src/eff_short_wordlist.dart';
part 'src/passwd_gen_base.dart';
